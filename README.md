# A simple calculator node module


Currently supported operators:

* addition (+)
* subtraction (-)
* multiplication (*)
* division (/)

Limitations:

* No support for grouping with parentheses
* You need to separate your numbers and operators with spaces
* Order of operations is a bit dodgy

### Usage

```
const Calculator = require('calculator')

const calc = new Calculator()
console.log(calc.eval("1 + 1"))    # -> 2
```

